##
##   _                         _             _
##  | |__  _ __ __ _ _ __   __| | ___  _ __ (_) __ _
##  | '_ \| '__/ _` | '_ \ / _` |/ _ \| '_ \| |/ _` |
##  | |_) | | | (_| | | | | (_| | (_) | | | | | (_| |
##  |_.__/|_|  \__,_|_| |_|\__,_|\___/|_| |_|_|\__,_|
##
##

#
# ~/.bashrc
#

# prompt
export PS1="[\[\033[01;34m\]\u\[$(tput sgr0)\]@\[\033[01;35m\]\h \[\033[36m\]\W\[$(tput sgr0)\]]\$ "

# path
PATH="$PATH:/home/brandonia/.local/bin"; export PATH

# real time bash history
shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

# set bash history limits
HISTSIZE=10000
HISTFILESIZE=10000

# alias
alias susp='systemctl suspend'
alias tmux='tmux -2'
alias ls='ls --color=auto'
alias grep='grep --color'
alias la='ls -al'
alias ll='ls -ll'

