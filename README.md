Ansible Role - common
===

A role that gets my most barebones environemnt up and running. Includes making sure my user exists, must have software is installed, copies the  and generate config files. 

## Requirements
- posix collection

## Variables
- `hostname` - define the hostname
- `username` - define username to be added
- `authorized_key` - absolute path of ssh public key file on command host
- `setup_ssh` - true or false, toggles ssh setup playbook